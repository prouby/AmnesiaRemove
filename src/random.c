/* Copyright (C) 2018-2024 by Pierre-Antoine Rouby */
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "random.h"

void
init_random(void)
{
  srand(time(NULL));
}

char
get_random_char (void)
{
  static float tmp;
  tmp = (float)rand() / (float)RAND_MAX;
  return ((char)(tmp * CHAR_MAX));
}
