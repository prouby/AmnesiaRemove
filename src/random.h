/* Copyright (C) 2018-2024 by Pierre-Antoine Rouby */
#ifndef RANDOM_H
#define RANDOM_H

void init_random(void);
char get_random_char(void);

#endif /* RANDOM_H */
