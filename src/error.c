/* Copyright (C) 2018 by Pierre-Antoine Rouby */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "error.h"

void
_error (char *msg, const char *src_file, const char *function, int line)
{
#ifdef DEBUG
  fprintf (stderr, "Error: [%s] %s:%d => %s: (%d)%s\n",
	   src_file, function, line, msg, errno, strerror(errno));
#else  /* DEBUG */
  fprintf (stderr, "arm: %s\n", strerror(errno));
#endif /* DEBUG */
  exit (EXIT_FAILURE);
}
