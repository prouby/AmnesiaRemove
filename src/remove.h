/* Copyright (C) 2018-2024 by Pierre-Antoine Rouby */
#ifndef REMOVE_H
#define REMOVE_H

#include "amnesia.h"

#define DEFAULT_PATH_SIZE 256

int overwrite (FILE *file, long len);
void _rmdir (const char *rep);
int remove_file (const char *file_path);
void recursive_remove (const char *rep);
bool interactive (const char *file);

#endif /* REMOVE_H */
