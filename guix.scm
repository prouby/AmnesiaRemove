;; Amnesia -- The UNIX rm command with overwrite options.
;; Copyright (C) 2018  Pierre-Antoine Rouby <pierre-antoine.rouby@inria.fr>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages pkg-config))

(package
  (name "amnesiaremove")
  (version "0.0.1")
  (source (local-file "." "amnesiaremove"
                      #:recursive? #t))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)))
  (home-page "https://framagit.org/prouby/AmnesiaRemove")
  (synopsis "UNIX rm command with overwrite options.")
  (description
   "This package provide @code{rm} comand with overwirte options, with
@code{arm} command.")
  (license license:gpl3+))
